using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Poolable))]
public class Enemy_misil_Controller : MonoBehaviour
{
    [SerializeField]
    public Transform m_target;

    public GameObject missile;

    private GameObjectPool objectPool;


    private Transform target;

    public float vidaActual = 3000;
    public float vidaMax = 3000;

    public ParticleSystem blood;

    void Start()
    {
        vidaActual = 3000;
        vidaMax = 3000;
        objectPool = GetComponent<GameObjectPool>();
        gameObject.GetComponent<Poolable>().Enable();
        StartCoroutine(ShotMissile());      
    }


    public void Init(Vector2 initialPosition, Transform pj_target)
    {
        gameObject.GetComponent<Poolable>().Enable();
        transform.position = initialPosition;
        this.target = pj_target;
    }

    // Update is called once per frame
    void Update()
    {
        if (vidaActual <= 0)
        {
            GetComponent<Poolable>().Disable();

        }
    }

    private void OnParticleCollision(GameObject other)
    {

        if (other.transform.tag == "explosion")
        {
            vidaActual -= 10f;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bala")
        {
            vidaActual -= 50f;
        }
    }
    IEnumerator ShotMissile()
    {
        while (true)
        {

            GameObject prefab = objectPool.GetElement();
            if (prefab != null)
            {
                prefab.GetComponent<Missile_Controller>().Init(new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1f),this.target);
            }
            yield return new WaitForSeconds(5f);
        }
    }
}
