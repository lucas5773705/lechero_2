using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeController : MonoBehaviour
{
    public Image fadeImage;
    public float fadeSpeed = 0.5f; 

    private float targetAlpha = 0f; 

    void Update()
    {
        fadeImage.color = Color.Lerp(fadeImage.color, new Color(0f, 0f, 0f, targetAlpha), fadeSpeed * Time.deltaTime);

    }

  
    public void StartFade()
    {
        targetAlpha = 1f;
        StartCoroutine(Dead(5f));
    }

    private IEnumerator Dead(float delayInSeconds)
    {

        yield return new WaitForSeconds(delayInSeconds);
        SceneManager.LoadScene("GameOver");
    }
}
