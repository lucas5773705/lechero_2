using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala_Controller : MonoBehaviour
{
    private Rigidbody2D rb;
    public float speed;
    private float sec_delete = 5f;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector3(speed,0f);
        Destroy(this.gameObject,sec_delete);

    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x > 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
    }
}
