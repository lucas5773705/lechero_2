using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class GrenadeController : MonoBehaviour
{
    public ParticleSystem particles_explosion;
    private int speed;
    private float jump;
    private float explosion = 5f;
    void Start()
    {
        Destroy(this.gameObject, explosion);    

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDestroy()
    {

        ParticleSystem particles_ = Instantiate(particles_explosion);
        particles_.transform.position = this.transform.position;
        particles_.Play();
        
    }
}
