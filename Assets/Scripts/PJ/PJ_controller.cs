using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PJ_controller : MonoBehaviour
{
    public static Transform playerTransform; 

    [SerializeField]
    private InputActionAsset m_Input;
    private InputActionAsset m_Gunfight;
    private InputAction m_movement;
    public float speed;
    private Vector2 moveInput;

    [SerializeField]
    private float jumpForce = 10f;
    private Rigidbody2D rb;

    public PJ_SO pj;
    public GameObject Bala;
    public Control_Vida barra_vida;

    private int saltos = 2;
    private bool salto = false;
    private bool flipped = false;
    private float tiempoUltimaReduccion;
    public float tiempoDeRecuperacion = 3f; // Tiempo de recuperaci�n despu�s de presionar Espacio
    public GameObject grenade;

    public Animator anim;
    private bool hit = false;
    public ParticleSystem pj_blood;
    public FadeController fadeController;

    public delegate void OnParticlesCollision(int numberOfParticles);
    public static event OnParticlesCollision ParticlesCollisionEvent;
    void Start()
    {
        this.gameObject.SetActive(true  );
        this.rb = GetComponent<Rigidbody2D>();
        speed = 2f;
        if (anim != null)
        {
            m_Input = Instantiate(m_Input);
            m_Input.FindActionMap("Movement").FindAction("Move").started += OnMove;
            m_Input.FindActionMap("Movement").FindAction("Move").performed += OnMove;
            m_Input.FindActionMap("Movement").FindAction("Move").canceled += OnCancelMove;
            m_Input.FindActionMap("Movement").FindAction("JUMP").performed += Jump;
            m_Input.FindActionMap("Movement").FindAction("Sprint").performed += onSprint;
            m_Input.FindActionMap("Movement").FindAction("Sprint").canceled += onSprintCancel;
            m_Input.FindActionMap("Movement").FindAction("Shoot").started += Shot;
            m_Input.FindActionMap("Movement").FindAction("Shoot").canceled += CancelShot;
            m_Input.FindActionMap("Movement").FindAction("Granada").started += ThrowGranade;
            m_Input.FindActionMap("Movement").FindAction("Granada").canceled += CancelThrowGranade;
            m_movement = m_Input.FindActionMap("Movement").FindAction("Move");
            m_Input.FindActionMap("Movement").Enable();
        }
    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Movement").FindAction("Move").started -= OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").performed -= OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").canceled -= OnCancelMove;
        m_Input.FindActionMap("Movement").FindAction("JUMP").performed -= Jump;
        m_Input.FindActionMap("Movement").FindAction("Sprint").performed -= onSprint;
        m_Input.FindActionMap("Movement").FindAction("Sprint").canceled -= onSprintCancel;
        m_Input.FindActionMap("Movement").FindAction("Shoot").started -= Shot;
        m_Input.FindActionMap("Movement").FindAction("Shoot").canceled -= CancelShot;
        m_Input.FindActionMap("Movement").FindAction("Granada").started -= ThrowGranade;
        m_Input.FindActionMap("Movement").FindAction("Granada").canceled -= CancelThrowGranade;
        m_Input.FindActionMap("Movement").Disable();
    }


    public void Awake()
    {
        playerTransform = transform;
        pj.vidaActual = 10000;
        pj.exp = 0;
    }

    private void Update()
    {
        moveInput = m_movement.ReadValue<Vector2>();
        if (anim != null && pj.vidaActual <= 0)
        {
            ParticleSystem blood = Instantiate(pj_blood);
            blood.transform.position = this.gameObject.transform.position;
            fadeController.StartFade();
            this.gameObject.SetActive(false);
            //StartCoroutine(Dead(5f));
            blood.Play();


        }

 
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (anim != null)
        {
            if (collision.transform.tag == "Suelo")
            {
                this.saltos = 2;
            }

            if (collision.transform.CompareTag("Enemy1"))
            {
                pj.vidaActual -= 100;
                anim.SetBool("Hit", true);
                hit = false;
            }

            if (collision.transform.tag == "Heart")
            {
                pj.vidaActual += 500;
                Destroy(collision.gameObject);
            }
        }
    }


    void OnCollisionExit2D(Collision2D collision)
    {
        if (anim != null)
        {
            if (collision.transform.CompareTag("Enemy1"))
            {
                anim.SetBool("Hit", false);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(moveInput.x * pj.speed, this.gameObject.GetComponent<Rigidbody2D>().velocity.y, 0f);

        if (this.gameObject.GetComponent<Rigidbody2D>().velocity.y < 0)
            this.rb.gravityScale = 3f;
        else
            this.rb.gravityScale = 1f;
    }

    public void Jump(InputAction.CallbackContext context)
    {
        if (anim != null)
        {
            if (this.saltos > 0)
            {
                print("HOLA");
                this.rb.AddForce(new Vector2(moveInput.x, jumpForce), ForceMode2D.Impulse);
                Debug.Log(this.gameObject.GetComponent<Rigidbody2D>().velocity);
                this.saltos--;
            }
        }
    }


    private void OnMove(InputAction.CallbackContext context)
    {
        if (anim != null)
        {
            anim.SetBool("Walk", true);
            if (moveInput.x < 0)
            {
                this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
                flipped = true;
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
                flipped = false;

            }
        }
    }

    private void OnCancelMove(InputAction.CallbackContext context)
    {
        if (anim != null)
        {
            moveInput = Vector2.zero;
            anim.SetBool("Walk", false);
        }

    }

    private void onSprint(InputAction.CallbackContext context)
    {
        if (anim != null)  
        {
            anim.SetBool("Run", true);
            this.pj.speed += 3f;
        }
    }

    private void onSprintCancel(InputAction.CallbackContext context)
    {
        if (anim != null)  
        {
            anim.SetBool("Run", false);
            this.pj.speed -= 3f;
        }
    }

    private void Shot(InputAction.CallbackContext context)
    {
        if (anim != null)
        {
            anim.SetBool("Shot", true);
            StartCoroutine(ShootCoroutine());
        }
    }

    private void CancelShot(InputAction.CallbackContext context)
    {
        if (anim != null)
        {
            anim.SetBool("Shot", false);
            StopAllCoroutines();
        }
    }


    private void ThrowGranade(InputAction.CallbackContext context)

    {
        if (anim != null)
        {
            GameObject _grenade = Instantiate(grenade);


            Vector2 throwDirection;
            if (this.flipped)
            {
                _grenade.transform.position = new Vector2(this.gameObject.transform.position.x - 0.2f, this.gameObject.transform.position.y);
                throwDirection = new Vector2(-0.2f, 0.3f);
            }
            else
            {
                _grenade.transform.position = new Vector2(this.gameObject.transform.position.x + 0.2f, this.gameObject.transform.position.y);

                throwDirection = new Vector2(0.2f, 0.3f);
            }

            _grenade.GetComponent<Rigidbody2D>().AddForce(throwDirection.normalized * 1f, ForceMode2D.Impulse);
        }
    }

    private void CancelThrowGranade(InputAction.CallbackContext context)
    {

    }


    IEnumerator ShootCoroutine()
    {
        while (true)
        {
            if (anim != null)
            {
                GameObject bala = Instantiate(Bala);

                if (this.gameObject.GetComponent<SpriteRenderer>().flipX == true)
                {
                    bala.GetComponent<Bala_Controller>().speed = -10f;
                    bala.GetComponent<Transform>().position = new Vector3(this.gameObject.transform.position.x - 0.5f, this.gameObject.transform.position.y + 0.2f, 0);
                }
                else
                {
                    bala.GetComponent<Transform>().position = new Vector3(this.gameObject.transform.position.x + 0.5f, this.gameObject.transform.position.y + 0.2f, 0);
                    bala.GetComponent<Bala_Controller>().speed = 10f;
                }
            }
            yield return new WaitForSeconds(0.25f);
        }
    }

    void OnParticleCollision(GameObject other)
    {

        if (other.transform.tag == "Blood")
        {
            int numberOfParticles = other.GetComponent<ParticleSystem>().particleCount;
            pj.exp += 1;
            ParticlesCollisionEvent?.Invoke(pj.exp);
        }

        if (other.transform.tag == "enemy_misil")
        {
            pj.vidaActual -= 1f;
        }

        if (other.transform.tag == "ExplosionLila")
        {
            pj.vidaActual -= 10f;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Heart")
        {
            pj.vidaActual += 100;
            Destroy(collision.gameObject);
        }
    }
    /*
    private IEnumerator Dead(float delayInSeconds)
    {
 
        yield return new WaitForSeconds(5f);
        this.gameObject.SetActive(false);
        SceneManager.LoadScene("Menu");
    }
    */
}


