using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud_Controller : MonoBehaviour
{

    private bool moviendomeAtras;
    private Rigidbody2D rb;

    void Start()
    {
        moviendomeAtras = false;
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (transform.position.x > 10f && !moviendomeAtras)
        {
            rb.velocity = new Vector2(-0.5f, 0f);
            moviendomeAtras = true;
        }
        else if (transform.position.x < -10f && moviendomeAtras)
        {
            rb.velocity = new Vector2(0.5f, 0f);
            moviendomeAtras = false;
        }
    }
}
