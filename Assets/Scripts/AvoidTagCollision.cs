using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidTagCollision : MonoBehaviour
{
    public string tagToAvoid;

    void Start()
    {
        // Encuentra todos los GameObjects con el mismo tag
        GameObject[] objectsToAvoid = GameObject.FindGameObjectsWithTag(tagToAvoid);

        foreach (GameObject obj in objectsToAvoid)
        {
            Collider2D thisCollider = GetComponent<Collider2D>();
            Collider2D otherCollider = obj.GetComponent<Collider2D>();

            if (thisCollider != null && otherCollider != null)
            {
                Physics2D.IgnoreCollision(thisCollider, otherCollider);
                print("CHOCAN");
            }
        }
    }
}
