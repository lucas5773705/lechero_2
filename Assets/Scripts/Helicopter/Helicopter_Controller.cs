using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Helicopter_Controller : MonoBehaviour
{
    public GameObjectPool enemyPool; 
    public float moveSpeed = 5f;
    public GameObject missilePrefab; // Asigna el prefab del misil en el inspector
    void Start()
    {
        StartCoroutine(shotMissile());
    }

    void Update()
    {
        if (enemyPool._Pool.Count > 0)
        {
            float targetX = enemyPool._Pool[0].transform.position.x;

            Vector3 targetPosition = new Vector3(targetX, transform.position.y, transform.position.z);
            if (targetPosition.x > this.transform.position.x)
            {
                this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
        }
    }


    IEnumerator shotMissile()
    {
        while (true)
        {
            if (enemyPool._Pool.Count > 0)
            {
                print("DEBERIA DISPARAR");
                Vector3 targetPosition = enemyPool._Pool[0].transform.position;
                Vector3 direction = (targetPosition - transform.position).normalized;

                GameObject missile = Instantiate(missilePrefab, transform.position, Quaternion.identity);

                Plane_Misil_Controller missileController = missile.GetComponent<Plane_Misil_Controller>();
                if (missileController != null)
                {
                    missileController.SetDirection(direction);
                }

                yield return new WaitForSeconds(7f);
            }
            else
            {
                yield return new WaitForSeconds(1f);
            }
        }
    }
}
