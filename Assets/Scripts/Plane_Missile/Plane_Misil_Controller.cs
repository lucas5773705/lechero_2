using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Plane_Misil_Controller : MonoBehaviour
{
    private Vector3 direction;
    public float missileSpeed = 10f;
    public ParticleSystem explosion;

    // M�todo para establecer la direcci�n del misil
    public void SetDirection(Vector3 newDirection)
    {
        direction = newDirection.normalized;
    }

    // M�todo para mover el misil en su direcci�n
    void Update()
    {
        transform.position += direction * missileSpeed * Time.deltaTime;
        // ... L�gica adicional del misil, como destrucci�n cuando colide ...
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        ParticleSystem particles_ = Instantiate(explosion);
        particles_.transform.position = this.transform.position;
        particles_.Play();
        Destroy(this.gameObject);
    }

}
