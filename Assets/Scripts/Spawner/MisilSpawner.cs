using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisilSpawner : Spawner_Controller
{

    public Transform pj_target;

    protected override void Spawn(GameObject prefab)
    {
        prefab.GetComponent<Enemy_misil_Controller>().Init(new Vector2(Random.Range(-9,9), 0f),pj_target);
    }
}
