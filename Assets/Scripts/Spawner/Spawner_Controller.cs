using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.EditorTools;
using UnityEngine;

[RequireComponent(typeof(GameObjectPool))]
public abstract class Spawner_Controller : MonoBehaviour
{

    [SerializeField]
    protected Transform ObjPosition;

    private GameObjectPool objectPool;
    void Start()
    {
        objectPool = GetComponent<GameObjectPool>();
        StartCoroutine(Spawner_Obj());
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Spawner_Obj()
    {
        while (true)
        {

            GameObject prefab = objectPool.GetElement();
            Spawn(prefab);
            yield return new WaitForSeconds(5f);
        }
    }

    protected abstract void Spawn(GameObject prefab);
}


