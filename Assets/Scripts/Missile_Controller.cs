using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Poolable))]
public class Missile_Controller : MonoBehaviour
{
    private bool dividido;
    private Transform pj;

    [SerializeField]
    private ParticleSystem particle;
    public void Init(Vector2 initialPosition, Transform targetPosition)
    {
        
        this.pj = targetPosition;
        gameObject.GetComponent<Poolable>().Enable();
        this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f,10f), ForceMode2D.Impulse);
        transform.position = initialPosition;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (this.gameObject.GetComponent<Rigidbody2D>().velocity.y < 0 && dividido)
        {
            print("SEPARARSE");
            Destroy(this.gameObject);
        }
    }


    public void Update()
    {
        Rigidbody2D rb2d = this.gameObject.GetComponent<Rigidbody2D>();
        if (rb2d != null && rb2d.velocity.y < 0)
        {
            Vector2 direccion = (pj.position - transform.position).normalized;
            transform.Translate(direccion * 8f * Time.deltaTime);
        }
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Suelo")
        {
            GetComponent<Poolable>().Disable();
            ParticleSystem _explosion_missile = Instantiate(particle);
            _explosion_missile.transform.position = this.gameObject.transform.position;
            _explosion_missile.Play();

        }
        else if (collision.transform.tag == "Player")
        {
            ParticleSystem _explosion_missile = Instantiate(particle);
            _explosion_missile.transform.position = this.gameObject.transform.position;
            _explosion_missile.Play();
        }
    }
}
