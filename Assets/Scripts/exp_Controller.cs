using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class exp_Controller : MonoBehaviour
{
    public PJ_SO pj;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        PJ_controller.ParticlesCollisionEvent += UpdateText;
    }

    void UpdateText(int numberOfParticles)
    {

        exp_Controller expController = GetComponent<exp_Controller>();
        if (expController != null)
        {
            GetComponent<TextMeshProUGUI>().text = "EXP: " + numberOfParticles.ToString();
        }

    }

    private void OnDestroy()
    {
        PJ_controller.ParticlesCollisionEvent -= UpdateText;
    }
}
