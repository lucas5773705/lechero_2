using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Poolable))]
public class EnemyController : MonoBehaviour
{

    public ParticleSystem blood;

    public float vidaActual = 1000;
    public float vidaMax = 1000;

    public float moveSpeed = 1f; 
    public float minX = -10f; 
    public float maxX = 10f; 
    private int direction = 1;
    public Animator anim;

    private bool aparicion = false;

    public GameObject lifeItemPrefab;
    public float lifeItemDropChance = 0.1f;

    public float damage = 100f;

    public PJ_SO pj;

    void Start()
    {
        vidaActual = 1000;
        vidaMax = 1000;
        gameObject.GetComponent<Poolable>().Enable();

    }

    // Update is called once per frame
    void Update()
    {
        float newX = transform.position.x + direction * moveSpeed * Time.deltaTime;

        newX = Mathf.Clamp(newX, minX, maxX);


        transform.position = new Vector2(newX, transform.position.y);

        if (newX >= maxX || newX <= minX)
        {
            ChangeDirection();
        }

        if (vidaActual <= 0)
        {
            Explosion_Blood();
            GetComponent<Poolable>().Disable();
            vidaActual = 1000;
            vidaMax = 1000;
            moveSpeed += 0.5f;
            damage += 250;
            float randomValue = Random.value;
            if (randomValue < lifeItemDropChance)
            {
                Instantiate(lifeItemPrefab, transform.position, Quaternion.identity);
            }
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Enemy1"))
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.collider);
        }
        if (collision.gameObject.CompareTag("Granada"))
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.collider);
        }
        if (collision.gameObject.CompareTag("Heart"))
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.collider);
        }
        if (collision.transform.tag == "Player")
        {
            pj.vidaActual -= damage;
        }
    }

    public void Init(Vector2 initialPosition)
    {
        gameObject.GetComponent<Poolable>().Enable();
        transform.position = initialPosition;
    }

    private void OnParticleCollision(GameObject other)
    {

        if (other.transform.tag == "explosion")
        {
            vidaActual -= 10f;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Bala")
        {
            vidaActual -= 50f;
            Destroy(collision.gameObject);
        }
    }

    public void Explosion_Blood()
    {
        ParticleSystem _blood = Instantiate(blood);
        _blood.transform.position = this.gameObject.transform.position;
        _blood.Play();
    }

    void ChangeDirection()
    {
        direction *= -1;
    }
}
