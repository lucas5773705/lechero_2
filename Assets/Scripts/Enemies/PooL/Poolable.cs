using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poolable : MonoBehaviour
{
    private GameObjectPool _pool;
    public GameObjectPool Pool
    {
        get
        {
            return _pool;
        }
        set
        {
            if(_pool == null)
                _pool = value;
        }
    }


    public void Enable()
    {
        gameObject.SetActive(true);
    }

    public void Disable()
    {
        _pool.ReturnElement(gameObject);
    }
}
