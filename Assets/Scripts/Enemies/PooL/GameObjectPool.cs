using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class GameObjectPool : MonoBehaviour
{
    [SerializeField]
    private int _defaultPoolSize = 10;
    [SerializeField]
    private GameObject _Prefab;
    public List<GameObject> _Pool = new List<GameObject>();

    private void Awake()
    {
        //Assert.IsNotNull(_Prefab.GetComponent<Poolable>());
        for (int i = 0; i < _defaultPoolSize; i++)
        {
            GameObject enemy = Instantiate(_Prefab, transform);
            enemy.SetActive(false);
            enemy.GetComponent<Poolable>().Pool = this;
            _Pool.Add(enemy);
        }

    }

    public GameObject GetElement()
    {
        GameObject objectToReturn = _Pool.FirstOrDefault<GameObject>(poolableObject => poolableObject.activeInHierarchy == false);
        if(objectToReturn != null)
        {
            objectToReturn.SetActive(true);
        }
        return objectToReturn;
    }


    public void ReturnElement(GameObject objectToReturn)
    {
        Assert.IsNotNull(_Pool.FirstOrDefault<GameObject>(poolableObject => poolableObject == objectToReturn));
        objectToReturn.SetActive(false);
    }



   

}
