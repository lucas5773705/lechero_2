using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Vida : MonoBehaviour
{
    public float vidaMaxima = 100f;
    public float vidaActual;

    private Vector3 escalaInicial;

    void Start()
    {
        vidaActual = vidaMaxima;
        escalaInicial = transform.localScale;
    }

    void Update()
    {
        // Actualiza la escala de la barra de vida basada en la vida actual
        float escalaX = Mathf.Clamp(vidaActual / vidaMaxima, 0f, 1f);
        transform.localScale = new Vector3(escalaX, transform.localScale.y, transform.localScale.z);

    }

    public void ReducirVida(float cantidad)
    {
        vidaActual -= cantidad;
        vidaActual = Mathf.Clamp(vidaActual, 0f, vidaMaxima);
    }
}
