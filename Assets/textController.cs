using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class textController : MonoBehaviour
{
    public PJ_SO pj;
    public TextMeshProUGUI textMeshPro;

    void Start()
    {
        // Aseg�rate de tener asignado el TextMeshPro en el Inspector
        if (textMeshPro != null && pj != null)
        {
            // Asigna el texto del ScriptableObject al TextMeshPro
            textMeshPro.text = "EXP: " + pj.exp.ToString();
        }
    }
}
