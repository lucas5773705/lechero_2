using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy_ScriptableObject", menuName = "ScriptableObjects/ENEMY_SO")]
public class Enemy_SO : ScriptableObject
{
    public float speed;
    public float vidaActual;
    public float vidaMax;
    public void Initialize(float maxVida)
    {
        vidaMax = maxVida;
        vidaActual = maxVida;
    }
}
