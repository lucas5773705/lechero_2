using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Rendering;

[CreateAssetMenu(fileName = "Pj_ScriptableObject", menuName = "ScriptableObjects/PJ_SO2", order = 1)]
public class PJ_SO : ScriptableObject
{
    public float speed;
    public float vidaActual;
    public float vidaMax;
    public int granadas;
    public int helicopter;
    public int round;
    public int exp;

}